
const swiper = new Swiper(".blog-slider", {
	spaceBetween: 30,
	effect: "fade",
	loop: true,
	mousewheel: {
		invert: false,
	},
	// autoHeight: true,
	pagination: {
		el: ".blog-slider__pagination",
		clickable: true,
	},
});
// home page accordion
$(document).ready(function(){
	$('.accordion-list > li > .answer').hide();
	  
	$('.accordion-list > li').click(function() {
	  if ($(this).hasClass("active")) {
		$(this).removeClass("active").find(".answer").slideUp();
	  } else {
		$(".accordion-list > li.active .answer").slideUp();
		$(".accordion-list > li.active").removeClass("active");
		$(this).addClass("active").find(".answer").slideDown();
	  }
	  return false;
	});
	
  });


